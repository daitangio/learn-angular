Only in angular6: .editorconfig
Only in angular6: angular.json
Only in angular4: bs-config.json
Only in angular6: e2e
Only in angular4: e2e-spec.ts
diff -w -y -r angular4/package.json angular6/package.json
{								{
  "name": "angular-io-example",					  "name": "angular-io-example",
  "version": "1.0.0",						  "version": "1.0.0",
  "private": true,						  "private": true,
  "description": "Example project from an angular.io guide.",	  "description": "Example project from an angular.io guide.",
  "scripts": {							  "scripts": {
    "test:once": "karma start karma.conf.js --single-run",    |	    "ng": "ng",
    "build": "tsc -p src/",				      |	    "build": "ng build --prod",
    "serve": "lite-server -c=bs-config.json",		      |	    "start": "ng serve",
    "prestart": "npm run build",			      |	    "test": "ng test",
    "start": "concurrently \"npm run build:watch\" \"npm run  |	    "lint": "tslint ./src/**/*.ts -t verbose",
    "pretest": "npm run build",				      |	    "e2e": "ng e2e"
    "test": "concurrently \"npm run build:watch\" \"karma sta <
    "pretest:once": "npm run build",			      <
    "build:watch": "tsc -p src/ -w",			      <
    "build:upgrade": "tsc",				      <
    "serve:upgrade": "http-server",			      <
    "build:aot": "ngc -p tsconfig-aot.json && rollup -c rollu <
    "serve:aot": "lite-server -c bs-config.aot.json",	      <
    "build:babel": "babel src -d src --extensions \".es6\" -- <
    "copy-dist-files": "node ./copy-dist-files.js",	      <
    "i18n": "ng-xi18n",					      <
    "lint": "tslint ./src/**/*.ts -t verbose"		      <
  },								  },
  "keywords": [],						  "keywords": [],
  "author": "",							  "author": "",
  "license": "MIT",						  "license": "MIT",
  "dependencies": {						  "dependencies": {
    "@angular/animations": "~4.3.1",			      |	    "@angular/animations": "^6.0.0",
    "@angular/common": "~4.3.1",			      |	    "@angular/common": "^6.0.0",
    "@angular/compiler": "~4.3.1",			      |	    "@angular/compiler": "^6.0.0",
    "@angular/compiler-cli": "~4.3.1",			      |	    "@angular/core": "^6.0.0",
    "@angular/core": "~4.3.1",				      |	    "@angular/forms": "^6.0.0",
    "@angular/forms": "~4.3.1",				      |	    "@angular/http": "^6.0.0",
    "@angular/http": "~4.3.1",				      |	    "@angular/platform-browser": "^6.0.0",
    "@angular/platform-browser": "~4.3.1",		      |	    "@angular/platform-browser-dynamic": "^6.0.0",
    "@angular/platform-browser-dynamic": "~4.3.1",	      |	    "@angular/router": "^6.0.0",
    "@angular/platform-server": "~4.3.1",		      |	    "@angular/upgrade": "^6.0.0",
    "@angular/router": "~4.3.1",			      |	    "angular-in-memory-web-api": "^0.6.0",
    "@angular/tsc-wrapped": "~4.3.1",			      |	    "core-js": "^2.5.4",
    "@angular/upgrade": "~4.3.1",			      |	    "rxjs": "^6.0.0",
    "angular-in-memory-web-api": "~0.4.0",		      |	    "zone.js": "^0.8.24"
    "core-js": "^2.4.1",				      <
    "rxjs": "^5.4.3",					      <
    "systemjs": "0.19.39",				      <
    "zone.js": "^0.8.4"					      <
  },								  },
  "devDependencies": {						  "devDependencies": {
    "@types/angular": "^1.5.16",			      |	    "@angular-devkit/build-angular": "~0.6.0",
    "@types/angular-animate": "^1.5.5",			      |	    "@angular/cli": "^6.0.0",
    "@types/angular-cookies": "^1.4.2",			      |	    "@angular/compiler-cli": "^6.0.0",
    "@types/angular-mocks": "^1.5.5",			      |	    "@angular/platform-server": "^6.0.0",
    "@types/angular-resource": "^1.5.6",		      |	    "@types/jasmine": "~2.8.0",
    "@types/angular-route": "^1.3.2",			      |	    "@types/jasminewd2": "^2.0.3",
    "@types/angular-sanitize": "^1.3.3",		      <
    "@types/jasmine": "2.5.36",				      <
    "@types/node": "^6.0.45",					    "@types/node": "^6.0.45",
    "babel-cli": "^6.16.0",				      |	    "jasmine-core": "~2.99.1",
    "babel-preset-angular2": "^0.0.2",			      |	    "jasmine-spec-reporter": "~4.2.1",
    "babel-preset-es2015": "^6.16.0",			      |	    "karma": "~1.7.1",
    "canonical-path": "0.0.2",				      |	    "karma-chrome-launcher": "~2.2.0",
    "concurrently": "^3.0.0",				      |	    "karma-coverage-istanbul-reporter": "~1.4.2",
    "http-server": "^0.9.0",				      |	    "karma-jasmine": "~1.1.1",
    "jasmine": "~2.4.1",				      <
    "jasmine-core": "~2.4.1",				      <
    "karma": "^1.3.0",					      <
    "karma-chrome-launcher": "^2.0.0",			      <
    "karma-cli": "^1.0.1",				      <
    "karma-jasmine": "^1.0.2",				      <
    "karma-jasmine-html-reporter": "^0.2.2",			    "karma-jasmine-html-reporter": "^0.2.2",
    "karma-phantomjs-launcher": "^1.0.2",			    "karma-phantomjs-launcher": "^1.0.2",
    "lite-server": "^2.2.2",				      <
    "lodash": "^4.16.2",					    "lodash": "^4.16.2",
    "phantomjs-prebuilt": "^2.1.7",				    "phantomjs-prebuilt": "^2.1.7",
    "protractor": "~5.1.0",				      |	    "protractor": "~5.3.0",
    "rollup": "^0.41.6",				      |	    "ts-node": "^5.0.1",
    "rollup-plugin-commonjs": "^8.0.2",			      |	    "tslint": "^5.9.1",
    "rollup-plugin-node-resolve": "2.0.0",		      |	    "typescript": "2.7.2",
    "rollup-plugin-uglify": "^1.0.1",			      |	    "jasmine-marbles": "^0.3.1"
    "source-map-explorer": "^1.3.2",			      <
    "tslint": "^3.15.1",				      <
    "typescript": "~2.3.2"				      <
  },								  },
  "repository": {}						  "repository": {}
}								}diff -w -y -r angular4/src/app/app.component.css angular6/src/app/app.component.css
							      >	/* AppComponent's private CSS styles */
h1 {								h1 {
  font-size: 1.2em;						  font-size: 1.2em;
  color: #999;							  color: #999;
  margin-bottom: 0;						  margin-bottom: 0;
}								}
h2 {								h2 {
  font-size: 2em;						  font-size: 2em;
  margin-top: 0;						  margin-top: 0;
  padding-top: 0;						  padding-top: 0;
}								}
nav a {								nav a {
  padding: 5px 10px;						  padding: 5px 10px;
  text-decoration: none;					  text-decoration: none;
  margin-top: 10px;						  margin-top: 10px;
  display: inline-block;					  display: inline-block;
  background-color: #eee;					  background-color: #eee;
  border-radius: 4px;						  border-radius: 4px;
}								}
nav a:visited, a:link {						nav a:visited, a:link {
  color: #607D8B;					      |	  color: #607d8b;
}								}
nav a:hover {							nav a:hover {
  color: #039be5;						  color: #039be5;
  background-color: #CFD8DC;				      |	  background-color: #cfd8dc;
}								}
nav a.active {							nav a.active {
  color: #039be5;						  color: #039be5;
}								}
Only in angular6/src/app: app.component.html
diff -w -y -r angular4/src/app/app.component.ts angular6/src/app/app.component.ts
import { Component } from '@angular/core';			import { Component } from '@angular/core';

@Component({							@Component({
  selector: 'my-app',					      |	  selector: 'app-root',
  template: `						      |	  templateUrl: './app.component.html',
    <h1>{{title}}</h1>					      |	  styleUrls: ['./app.component.css']
    <nav>						      <
      <a routerLink="/dashboard" routerLinkActive="active">Da <
      <a routerLink="/heroes" routerLinkActive="active">Heroe <
    </nav>						      <
    <router-outlet></router-outlet>			      <
  `,							      <
  styleUrls: ['./app.component.css'],			      <
})								})
export class AppComponent {					export class AppComponent {
  title = 'Tour of Heroes';					  title = 'Tour of Heroes';
}								}
diff -w -y -r angular4/src/app/app.module.ts angular6/src/app/app.module.ts
import { NgModule }       from '@angular/core';			import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';	import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';		import { FormsModule }    from '@angular/forms';

import { AppComponent }         from './app.component';		import { AppComponent }         from './app.component';
import { DashboardComponent }   from './dashboard.component'; |	import { DashboardComponent }   from './dashboard/dashboard.c
import { HeroDetailComponent }  from './hero-detail.component |	import { HeroDetailComponent }  from './hero-detail/hero-deta
import { HeroesComponent }      from './heroes.component';    |	import { HeroesComponent }      from './heroes/heroes.compone
import { HeroService }          from './hero.service';	      |	import { MessagesComponent }    from './messages/messages.com

import { AppRoutingModule }     from './app-routing.module';	import { AppRoutingModule }     from './app-routing.module';

@NgModule({							@NgModule({
  imports: [							  imports: [
    BrowserModule,						    BrowserModule,
    FormsModule,						    FormsModule,
    AppRoutingModule						    AppRoutingModule
  ],								  ],
  declarations: [						  declarations: [
    AppComponent,						    AppComponent,
    DashboardComponent,						    DashboardComponent,
							      >	    HeroesComponent,
    HeroDetailComponent,					    HeroDetailComponent,
    HeroesComponent					      |	    MessagesComponent
  ],								  ],
  providers: [ HeroService ],				      <
  bootstrap: [ AppComponent ]					  bootstrap: [ AppComponent ]
})								})
export class AppModule { }					export class AppModule { }
diff -w -y -r angular4/src/app/app-routing.module.ts angular6/src/app/app-routing.module.ts
import { NgModule }             from '@angular/core';		import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';		import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent }   from './dashboard.component'; |	import { DashboardComponent }   from './dashboard/dashboard.c
import { HeroesComponent }      from './heroes.component';    |	import { HeroesComponent }      from './heroes/heroes.compone
import { HeroDetailComponent }  from './hero-detail.component |	import { HeroDetailComponent }  from './hero-detail/hero-deta

const routes: Routes = [					const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },	  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard',  component: DashboardComponent },	  { path: 'dashboard', component: DashboardComponent },
  { path: 'detail/:id', component: HeroDetailComponent },	  { path: 'detail/:id', component: HeroDetailComponent },
  { path: 'heroes',     component: HeroesComponent }		  { path: 'heroes', component: HeroesComponent }
];								];

@NgModule({							@NgModule({
  imports: [ RouterModule.forRoot(routes) ],			  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]					  exports: [ RouterModule ]
})								})
export class AppRoutingModule {}				export class AppRoutingModule {}
Only in angular6/src/app: dashboard
Only in angular4/src/app: dashboard.component.css
Only in angular4/src/app: dashboard.component.html
Only in angular4/src/app: dashboard.component.ts
diff -w -y -r angular4/src/app/hero.service.ts angular6/src/app/hero.service.ts
							      >	import { Injectable } from '@angular/core';
							      >
							      >	import { Observable, of } from 'rxjs';
							      >
import { Hero } from './hero';					import { Hero } from './hero';
import { HEROES } from './mock-heroes';				import { HEROES } from './mock-heroes';
import { Injectable } from '@angular/core';		      |	import { MessageService } from './message.service';

@Injectable()						      |	@Injectable({ providedIn: 'root' })
export class HeroService {					export class HeroService {
  getHeroes(): Promise<Hero[]> {			      <
    return Promise.resolve(HEROES);			      <
  }							      <

  getHeroesSlowly(): Promise<Hero[]> {			      |	  constructor(private messageService: MessageService) { }
    return new Promise(resolve => {			      |
      // Simulate server latency with 2 second delay	      |	  getHeroes(): Observable<Hero[]> {
      setTimeout(() => resolve(this.getHeroes()), 2000);      |	    // TODO: send the message _after_ fetching the heroes
    });							      |	    this.messageService.add('HeroService: fetched heroes');
							      >	    return of(HEROES);
  }								  }

  getHero(id: number): Promise<Hero> {			      |	  getHero(id: number): Observable<Hero> {
    return this.getHeroes()				      |	    // TODO: send the message _after_ fetching the hero
               .then(heroes => heroes.find(hero => hero.id == |	    this.messageService.add(`HeroService: fetched hero id=${i
							      >	    return of(HEROES.find(hero => hero.id === id));
  }								  }
}								}
diff -w -y -r angular4/src/app/hero.ts angular6/src/app/hero.ts
export class Hero {						export class Hero {
  id: number;							  id: number;
  name: string;							  name: string;
}								}
Only in angular6/src/app: hero-detail
Only in angular4/src/app: hero-detail.component.css
Only in angular4/src/app: hero-detail.component.html
Only in angular4/src/app: hero-detail.component.ts
Only in angular6/src/app: heroes
Only in angular4/src/app: heroes.component.css
Only in angular4/src/app: heroes.component.html
Only in angular4/src/app: heroes.component.ts
Only in angular6/src/app: message.service.spec.ts
Only in angular6/src/app: message.service.ts
Only in angular6/src/app: messages
diff -w -y -r angular4/src/app/mock-heroes.ts angular6/src/app/mock-heroes.ts
import { Hero } from './hero';					import { Hero } from './hero';

export const HEROES: Hero[] = [					export const HEROES: Hero[] = [
  { id: 11, name: 'Mr. Nice' },					  { id: 11, name: 'Mr. Nice' },
  { id: 12, name: 'Narco' },					  { id: 12, name: 'Narco' },
  { id: 13, name: 'Bombasto' },					  { id: 13, name: 'Bombasto' },
  { id: 14, name: 'Celeritas' },				  { id: 14, name: 'Celeritas' },
  { id: 15, name: 'Magneta' },					  { id: 15, name: 'Magneta' },
  { id: 16, name: 'RubberMan' },				  { id: 16, name: 'RubberMan' },
  { id: 17, name: 'Dynama' },					  { id: 17, name: 'Dynama' },
  { id: 18, name: 'Dr IQ' },					  { id: 18, name: 'Dr IQ' },
  { id: 19, name: 'Magma' },					  { id: 19, name: 'Magma' },
  { id: 20, name: 'Tornado' }					  { id: 20, name: 'Tornado' }
];								];
Only in angular6/src: browserslist
Only in angular6/src: environments
Only in angular6/src: favicon.ico
diff -w -y -r angular4/src/index.html angular6/src/index.html
<!DOCTYPE html>						      |	<!doctype html>
<html>							      |	<html lang="en">
  <head>							<head>
							      >	  <meta charset="utf-8">
							      >	  <title>Tour of Heroes</title>
    <base href="/">						  <base href="/">
    <title>Angular Tour of Heroes</title>		      <
    <meta charset="UTF-8">				      <
    <meta name="viewport" content="width=device-width, initia <
							      <
    <link rel="stylesheet" href="styles.css">		      <
    <!-- Polyfills -->					      <
    <script src="node_modules/core-js/client/shim.min.js"></s <
							      <
    <script src="node_modules/zone.js/dist/zone.js"></script> <
    <script src="node_modules/systemjs/dist/system.src.js"></ <

    <script src="systemjs.config.js"></script>		      |	  <meta name="viewport" content="width=device-width, initial-
    <script>						      |	  <link rel="icon" type="image/x-icon" href="favicon.ico">
      System.import('main.js').catch(function(err){ console.e <
    </script>						      <
  </head>							</head>
							      <
  <body>							<body>
    <my-app>Loading...</my-app>				      |	  <app-root></app-root>
  </body>							</body>
</html>								</html>
Only in angular6/src: karma.conf.js
diff -w -y -r angular4/src/main.ts angular6/src/main.ts
// main entry point					      |	import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-bro	import { platformBrowserDynamic } from '@angular/platform-bro
							      >
import { AppModule } from './app/app.module';			import { AppModule } from './app/app.module';
							      >	import { environment } from './environments/environment';
							      >
							      >	if (environment.production) {
							      >	  enableProdMode();
							      >	}

platformBrowserDynamic().bootstrapModule(AppModule);		platformBrowserDynamic().bootstrapModule(AppModule);
Only in angular6/src: polyfills.ts
diff -w -y -r angular4/src/styles.css angular6/src/styles.css
/* Master Styles */						/* Master Styles */
h1 {								h1 {
  color: #369;							  color: #369;
  font-family: Arial, Helvetica, sans-serif;			  font-family: Arial, Helvetica, sans-serif;
  font-size: 250%;						  font-size: 250%;
}								}
h2, h3 {							h2, h3 {
  color: #444;							  color: #444;
  font-family: Arial, Helvetica, sans-serif;			  font-family: Arial, Helvetica, sans-serif;
  font-weight: lighter;						  font-weight: lighter;
}								}
body {								body {
  margin: 2em;							  margin: 2em;
}								}
body, input[text], button {					body, input[text], button {
  color: #888;							  color: #888;
  font-family: Cambria, Georgia;				  font-family: Cambria, Georgia;
}								}
a {								a {
  cursor: pointer;						  cursor: pointer;
  cursor: hand;							  cursor: hand;
}								}
button {							button {
  font-family: Arial;						  font-family: Arial;
  background-color: #eee;					  background-color: #eee;
  border: none;							  border: none;
  padding: 5px 10px;						  padding: 5px 10px;
  border-radius: 4px;						  border-radius: 4px;
  cursor: pointer;						  cursor: pointer;
  cursor: hand;							  cursor: hand;
}								}
button:hover {							button:hover {
  background-color: #cfd8dc;					  background-color: #cfd8dc;
}								}
button:disabled {						button:disabled {
  background-color: #eee;					  background-color: #eee;
  color: #aaa;							  color: #aaa;
  cursor: auto;							  cursor: auto;
}								}

/* Navigation link styles */					/* Navigation link styles */
nav a {								nav a {
  padding: 5px 10px;						  padding: 5px 10px;
  text-decoration: none;					  text-decoration: none;
  margin-right: 10px;						  margin-right: 10px;
  margin-top: 10px;						  margin-top: 10px;
  display: inline-block;					  display: inline-block;
  background-color: #eee;					  background-color: #eee;
  border-radius: 4px;						  border-radius: 4px;
}								}
nav a:visited, a:link {						nav a:visited, a:link {
  color: #607D8B;						  color: #607D8B;
}								}
nav a:hover {							nav a:hover {
  color: #039be5;						  color: #039be5;
  background-color: #CFD8DC;					  background-color: #CFD8DC;
}								}
nav a.active {							nav a.active {
  color: #039be5;						  color: #039be5;
}								}

/* everywhere else */						/* everywhere else */
* {								* {
  font-family: Arial, Helvetica, sans-serif;			  font-family: Arial, Helvetica, sans-serif;
}								}
Only in angular4/src: systemjs.config.js
Only in angular4/src: systemjs-angular-loader.js
Only in angular6/src: test.ts
Only in angular6/src: tsconfig.app.json
Only in angular4/src: tsconfig.json
Only in angular6/src: tsconfig.spec.json
Only in angular6/src: tslint.json
Only in angular6: tsconfig.json
diff -w -y -r angular4/tslint.json angular6/tslint.json
{								{
							      >	  "rulesDirectory": [
							      >	    "node_modules/codelyzer"
							      >	  ],
  "rules": {							  "rules": {
							      >	    "arrow-return-shorthand": true,
							      >	    "callable-types": true,
    "class-name": true,						    "class-name": true,
    "comment-format": [						    "comment-format": [
      true,							      true,
      "check-space"						      "check-space"
    ],								    ],
    "curly": true,						    "curly": true,
							      >	    "deprecation": {
							      >	      "severity": "warn"
							      >	    },
    "eofline": true,						    "eofline": true,
    "forin": true,						    "forin": true,
							      >	    "import-blacklist": [
							      >	      true,
							      >	      "rxjs",
							      >	      "rxjs/Rx"
							      >	    ],
							      >	    "import-spacing": true,
    "indent": [							    "indent": [
      true,							      true,
      "spaces"							      "spaces"
    ],								    ],
							      >	    "interface-over-type-literal": true,
    "label-position": true,					    "label-position": true,
    "label-undefined": true,				      <
    "max-line-length": [					    "max-line-length": [
      true,							      true,
      140							      140
    ],								    ],
    "member-access": false,					    "member-access": false,
    "member-ordering": [					    "member-ordering": [
      true,							      true,
      "static-before-instance",				      |	      {
      "variables-before-functions"			      |	        "order": [
							      >	          "static-field",
							      >	          "instance-field",
							      >	          "static-method",
							      >	          "instance-method"
							      >	        ]
							      >	      }
    ],								    ],
    "no-arg": true,						    "no-arg": true,
    "no-bitwise": true,						    "no-bitwise": true,
    "no-console": [						    "no-console": [
      true,							      true,
      "debug",							      "debug",
      "info",							      "info",
      "time",							      "time",
      "timeEnd",						      "timeEnd",
      "trace"							      "trace"
    ],								    ],
    "no-construct": true,					    "no-construct": true,
    "no-debugger": true,					    "no-debugger": true,
    "no-duplicate-key": true,				      |	    "no-duplicate-super": true,
    "no-duplicate-variable": true,			      <
    "no-empty": false,						    "no-empty": false,
							      >	    "no-empty-interface": true,
    "no-eval": true,						    "no-eval": true,
    "no-inferrable-types": true,			      |	    "no-inferrable-types": [
							      >	      true,
							      >	      "ignore-params"
							      >	    ],
							      >	    "no-misused-new": true,
							      >	    "no-non-null-assertion": true,
    "no-shadowed-variable": true,				    "no-shadowed-variable": true,
    "no-string-literal": false,					    "no-string-literal": false,
							      >	    "no-string-throw": true,
    "no-switch-case-fall-through": true,			    "no-switch-case-fall-through": true,
    "no-trailing-whitespace": true,				    "no-trailing-whitespace": true,
							      >	    "no-unnecessary-initializer": true,
    "no-unused-expression": true,				    "no-unused-expression": true,
    "no-unused-variable": true,				      <
    "no-unreachable": true,				      <
    "no-use-before-declare": true,				    "no-use-before-declare": true,
    "no-var-keyword": true,					    "no-var-keyword": true,
    "object-literal-sort-keys": false,				    "object-literal-sort-keys": false,
    "one-line": [						    "one-line": [
      true,							      true,
      "check-open-brace",					      "check-open-brace",
      "check-catch",						      "check-catch",
      "check-else",						      "check-else",
      "check-whitespace"					      "check-whitespace"
    ],								    ],
							      >	    "prefer-const": true,
    "quotemark": [						    "quotemark": [
      true,							      true,
      "single"							      "single"
    ],								    ],
    "radix": true,						    "radix": true,
    "semicolon": [						    "semicolon": [
							      >	      true,
      "always"							      "always"
    ],								    ],
    "triple-equals": [						    "triple-equals": [
      true,							      true,
      "allow-null-check"					      "allow-null-check"
    ],								    ],
    "typedef-whitespace": [					    "typedef-whitespace": [
      true,							      true,
      {								      {
        "call-signature": "nospace",				        "call-signature": "nospace",
        "index-signature": "nospace",				        "index-signature": "nospace",
        "parameter": "nospace",					        "parameter": "nospace",
        "property-declaration": "nospace",			        "property-declaration": "nospace",
        "variable-declaration": "nospace"			        "variable-declaration": "nospace"
      }								      }
    ],								    ],
							      >	    "typeof-compare": true,
							      >	    "unified-signatures": true,
    "variable-name": false,					    "variable-name": false,
    "whitespace": [						    "whitespace": [
      true,							      true,
      "check-branch",						      "check-branch",
      "check-decl",						      "check-decl",
      "check-operator",						      "check-operator",
      "check-separator",					      "check-separator",
      "check-type"						      "check-type"
    ]							      |	    ],
							      >	    "directive-selector": [
							      >	      true,
							      >	      "attribute",
							      >	      "app",
							      >	      "camelCase"
							      >	    ],
							      >	    "component-selector": [
							      >	      true,
							      >	      "element",
							      >	      "app",
							      >	      "kebab-case"
							      >	    ],
							      >	    "no-output-on-prefix": true,
							      >	    "use-input-property-decorator": true,
							      >	    "use-output-property-decorator": true,
							      >	    "use-host-property-decorator": true,
							      >	    "no-input-rename": true,
							      >	    "no-output-rename": true,
							      >	    "use-life-cycle-interface": true,
							      >	    "use-pipe-transform-interface": true,
							      >	    "component-class-suffix": true,
							      >	    "directive-class-suffix": true
  }								  }
}								}
