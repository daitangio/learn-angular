import { Injectable  } from '@angular/core';
import { Faq }  from './faq';

const DATA: Faq[] = [
    {
        id:1,
        question: "How to survive to nodejs?",
        answer: "Study a lot, and learn TypeScript"
    },
    {
        id:2,
        question: "What is the best way to solve a issue on Angular?",
        answer: "Copy the error on google, and then look for the stackoverflow answer"
    },
    {
        id:3,
        question: "All this *ngIf *ngFor, what damn mean?",
        answer: "Look at https://v4.angular.io/guide/structural-directives to have a nice day"
    }
];
/**
   GG: Study http://exploringjs.com/es6/ch_promises.html
*/
@Injectable()
export class FaqService{
    getFaqs(): Promise<Faq[]>  {
        return new Promise( (myResolve, myReject) => {
            //setTimeout(() => myReject('BAD'),200);
                            
            setTimeout(() => myResolve(DATA),600);
        });
    }
}
