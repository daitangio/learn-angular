"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var DATA = [
    {
        id: 1,
        question: "How to survive to nodejs?",
        answer: "Study a lot, and learn TypeScript"
    },
    {
        id: 2,
        question: "What is the best way to solve a issue on Angular?",
        answer: "Copy the error on google, and then look for the stackoverflow answer"
    },
    {
        id: 3,
        question: "All this *ngIf *ngFor, what damn mean?",
        answer: "Look at https://v4.angular.io/guide/structural-directives to have a nice day"
    }
];
/**
   GG: Study http://exploringjs.com/es6/ch_promises.html
*/
var FaqService = (function () {
    function FaqService() {
    }
    FaqService.prototype.getFaqs = function () {
        return new Promise(function (myResolve, myReject) {
            //setTimeout(() => myReject('BAD'),200);
            setTimeout(function () { return myResolve(DATA); }, 600);
        });
    };
    return FaqService;
}());
FaqService = __decorate([
    core_1.Injectable()
], FaqService);
exports.FaqService = FaqService;
//# sourceMappingURL=faq.service.js.map