// Faq Model
export class Faq {
    id: number;
    question: string;
    answer: string;
}
