import { Component , OnInit  } from '@angular/core';
import { FaqService } from './faq.service';
import { Faq }  from './faq';







@Component({
    // GG: 'Manual' Wiring: link the service provider here
    providers: [FaqService],
    selector: 'my-app',
styles: [`
  .selected {
    background-color: #CFD8DC !important;
    color: white;
  }
  .faqs {
    margin: 0 0 2em 0;
    
    padding: 0;
    width: 25em;
  }
  .faqs li {
    cursor: pointer;
    position: relative;
    left: 0;
    background-color: #EEE;
    margin: .5em;
    padding: .3em 0;
    height: 1.6em;
    border-radius: 4px;
  }
  .faqs li.selected:hover {
    background-color: #BBD8DC !important;
    color: white;
  }
  .faqs li:hover {
    color: #607D8B;
    background-color: #DDD;
    left: .1em;
  }
  .faqs .text {
    position: relative;
    top: -3px;
  }
  .faqs .badge {
    display: inline-block;
    font-size: small;
    color: white;
    padding: 0.8em 0.7em 0 0.7em;
    background-color: #607D8B;
    line-height: 1em;
    position: relative;
    left: -1px;
    top: -4px;
    height: 1.8em;
    margin-right: .8em;
    border-radius: 4px 0 0 4px;
  }
`],

// [(ngModel)] is the bidirectional binding
// The (*) prefix to ngFor indicates that the <li> element and its children constitute a master template.
    // styleUrls: [ './heroes.component.css' ]
    templateUrl: `./app.component.html`
})

export class AppComponent implements OnInit {
    title = 'Frequently Asked Questions';
    faqList: Faq[];
    selectedFaq: Faq;

    // GG: Build automatically a this.faqService
    // Angular will inject it 
    constructor(private faqService: FaqService) {
    }
    
    ngOnInit(): void {
        console.log("INIT: "+JSON.stringify(this));
        this.faqService.getFaqs().then( list =>  {
            this.faqList=list;
            console.log("Faq Loaded!");            
        }).catch(error => alert("Error:"+error))
    }
    onSelect(myfaq: Faq): void {
        this.selectedFaq = myfaq;
        console.log("Selected Faq:"+this.selectedFaq.question);
    }
}

