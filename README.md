# Projects
A set of project(s) for learning Angular.
You can find the Hero tutorial up to "Routing" example for angular 4,5,6 with a
comparasion versus angular 4

## Faq Angular4
It is a simple Frequently Asked Question page adapter in plain-vanilla angular 4:

1. You can search faq
2. You can organize Faq in hierarchy [TODO]

## The Right mix
Installing a legacy angular4 application from scratch seems a bit too
difficult.

Angular Cli will force you with the last version (i.e. Angular 6 or
5).

Also, the package-lock.json seems very important to track down the
dependency (so we ask ourself why package.json is not enoug!):

``` 
package-lock.json is automatically generated for any operations where npm modifies either the node_modules tree, or package.json. It describes the exact tree that was generated, such that subsequent installs are able to generate identical trees, regardless of intermediate dependency updates.

This file is intended to be committed into source repositories, and
serves various purposes [...]
``` 
See https://docs.npmjs.com/files/package-lock.json
for more fantasy-pumped ideas.
Anyway I managed to find out the correct mix of angular, angular cli,
typescript and so on.

### Node Versions

1. Node: v8.11.3
2. Npm: 6.1.0

  
# Contributing
Send an email to incoming+daitangio/learn-angular@incoming.gitlab.com
for bugs, improvements request and so on.


# Dev
https://v4.angular.io/docs


## Audit
Run `npm i npm@latest -g` to upgrade your npm version, and then `npm audit` 

## TODO 
Try to increase versions to
"@angular/core": "^4.4.7",
"@angular/cli": "~1.4.8",
"@angular/compiler-cli": "^4.4.7",


